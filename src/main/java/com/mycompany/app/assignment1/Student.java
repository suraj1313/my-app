package com.assignment1;

public class Student {

    private String stdName;
    private String stdRollNo;

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        Student student = (Student)obj;
        return getStdRollNo().equals(student.getStdRollNo()) && getStdName().equals(student.getStdName());
    }


    public String getStdName() {
        return stdName;
    }

    public void setStdName(String stdName) {
        this.stdName = stdName;
    }

    public String getStdRollNo() {
        return stdRollNo;
    }

    public void setStdRollNo(String stdRollNo) {
        this.stdRollNo = stdRollNo;
    }
}
