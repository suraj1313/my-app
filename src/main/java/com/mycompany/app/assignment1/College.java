package com.assignment1;

import java.util.List;

public class College {

    private String cllName;
    private String cllAddress;
    List<StdClass> list;

    @Override
    public boolean equals(Object obj2){
        if(this == obj2) return true;
        if(obj2 == null || getClass() != obj2.getClass()) return false;
        College college = (College) obj2;
        return getCllName().equals(college.getCllName()) && getCllAddress().equals(college.getCllAddress()) && getList().equals(college.getList());
    }

    public String getCllName() {
        return cllName;
    }

    public void setCllName(String cllName) {
        this.cllName = cllName;
    }

    public String getCllAddress() {
        return cllAddress;
    }

    public void setCllAddress(String cllAddress) {
        this.cllAddress = cllAddress;
    }

    public List<StdClass> getList() {
        return list;
    }

    public void setList(List<StdClass> list) {
        this.list = list;
    }
}
