package com.assignment1;

import java.util.Set;

public class StdClass {
    private String clsName;
    private String totalNoOfStd;
    Set<Student> set;

    @Override
    public boolean equals(Object obj1) {
        if(this == obj1) return true;
        if(obj1 == null || getClass() != obj1.getClass()) return false;
        StdClass cls = (StdClass) obj1;
        return getClsName().equals(cls.getClsName()) && getTotalNoOfStd().equals(cls.getTotalNoOfStd()) && getSet().equals(cls.getSet());
    }


    public String getClsName() {
        return clsName;
    }

    public void setClsName(String clsName) {
        this.clsName = clsName;
    }

    public String getTotalNoOfStd() {
        return totalNoOfStd;
    }

    public void setTotalNoOfStd(String totalNoOfStd) {
        this.totalNoOfStd = totalNoOfStd;
    }

    public Set<Student> getSet() {
        return set;
    }

    public void setSet(Set<Student> set) {
        this.set = set;
    }
}
