package com.assignment1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestStudent {
    public static void main(String[] args) {

        Student std = new Student();
        std.setStdName("suraj");
        std.setStdRollNo("12");
        Student std1 = new Student();
        std1.setStdName("rah");
        std1.setStdRollNo("23");

        System.out.println(std.equals(std1));

        StdClass cl = new StdClass();
        Set<Student> s = new HashSet<Student>();
        s.add(std);
        cl.setClsName("ten");
        cl.setTotalNoOfStd("2345");
        cl.setSet(s);

        StdClass cl1 = new StdClass();
        Set<Student> s1 = new HashSet<Student>();
        s1.add(std);
        cl1.setClsName("twelve");
        cl1.setTotalNoOfStd("23245");
        cl1.setSet(s1);

        System.out.println(cl.equals(cl));
        System.out.println(cl.equals(cl1));

        College clg = new College();
        List<StdClass> st = new ArrayList<StdClass>();
        st.add(cl);
        clg.setCllName("Ku");
        clg.setCllAddress("Assam");
        clg.setList(st);

        College clg1 = new College();
        List<StdClass> st1 = new ArrayList<StdClass>();
        st1.add(cl);
        clg1.setCllName("ADU");
        clg1.setCllAddress("Assam");
        clg1.setList(st1);

        System.out.println(clg.equals(clg1));
        System.out.println(clg.equals(clg));

    }
}
