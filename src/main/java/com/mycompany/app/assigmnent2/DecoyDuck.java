package com.assigmnent2;

public class DecoyDuck extends Duck {
    public DecoyDuck(FlyBehaviour flyBehaviour, QuackBehaviour quackBehaviour) {
        super(flyBehaviour, quackBehaviour);
    }
}
