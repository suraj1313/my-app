package com.assigmnent2;

public class BrownDuck extends Duck {
    public BrownDuck(FlyBehaviour flyBehaviour, QuackBehaviour quackBehaviour) {
        super(flyBehaviour, quackBehaviour);
    }
}
