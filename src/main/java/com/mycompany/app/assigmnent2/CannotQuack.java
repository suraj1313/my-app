package com.assigmnent2;

public class CannotQuack implements QuackBehaviour{
    @Override
    public void quack() {
        System.out.println("I cannot quack");
    }
}
