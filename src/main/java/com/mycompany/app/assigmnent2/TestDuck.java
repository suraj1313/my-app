package com.assigmnent2;

public class TestDuck {
    public static void main(String [] args) {

                FlyBehaviour canFly = new CanFly();
                FlyBehaviour cannotFly = new CannotFly();

                QuackBehaviour canQuack = new CanQuack();
                QuackBehaviour cannotQuack = new CannotQuack();

                Duck wd = new WhiteDuck(canFly, canQuack);
                Duck bd = new BrownDuck(canFly, canQuack);
                Duck md = new MalladDuck(canFly, canQuack);
                Duck rd = new RubberDuck(cannotFly, canQuack);
                Duck dd = new DecoyDuck(cannotFly, cannotQuack);

                System.out.println("White Duck Behaviour :");
                wd.fly();
                wd.quack();

                System.out.println("Brown Duck Behaviour :");
                bd.fly();
                bd.quack();

                System.out.println("Mallad Duck Behaviour :");
                md.fly();
                md.quack();

                System.out.println("Rubber Duck Behaviour :");
                 rd.fly();
                 rd.quack();

                System.out.println("Decoy Duck Behaviour :");
                dd.fly();
                dd.quack();
    }
}
